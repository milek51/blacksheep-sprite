# -*- coding: utf-8 -*-

from django import forms

from django.conf import settings

import zipfile
import os
import shutil

def handle_uploaded_and_clear_file(f, clear = False):
    hashName = os.urandom(16).encode("hex")
    path = "%s/uploaded/%s/" % (settings.MEDIA_ROOT, hashName)

    if not clear:
        if f.size > 10485760:
            return (False, u"Maximum limit size of archive (10 Mb).")

        if os.path.splitext(f.name)[1] not in (".zip",):
            return (False, u"Wrong file extension (only zip file supported).")

        if not os.path.exists(path):
            os.mkdir(path)

        destination = open("%s%s" % (path, f.name), "wb+")

        for chunk in f.chunks():
            destination.write(chunk)

        destination.close()

        try:
            fzip = zipfile.ZipFile("%s%s" % (path, f), "r")
        except zipfile.BadZipfile:
            shutil.rmtree(path)
            return (False, u"Wrong file extension (only zip file supported).")

        return (True, "media/uploaded/%s/%s" % (hashName, f.name))
    else:
        os.remove("%s/%s" % (path, f))

class AddZip(forms.Form):
    zipf = forms.FileField(required = True)
    captcha = forms.CharField(max_length = 10, widget = forms.TextInput(attrs = {"title" : u"Rewrite token", "id" : "captcha", "placeholder" : "Rewrite token"}))