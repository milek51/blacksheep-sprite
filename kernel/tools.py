# -*- coding: utf-8 -*-

from PIL import Image

from PIL import ImageDraw
from PIL import ImageFont

import cStringIO
import hashlib

import string
import random

from hashlib import sha1

from django.conf import settings

from django.template.defaultfilters import slugify

def _slugify(text):
    return slugify(text.replace(u"ł", u"l").replace(u"Ł", u"L"))

def captchaImage(width, height, length, fontSize):
    imgtext = "".join([random.choice(string.ascii_letters + string.digits + "-+!@%?&#$=") for x in xrange(length)])
    imghash = hashlib.sha1(imgtext).hexdigest()
    img = Image.new("RGB", (width, height), "#888")
    draw = ImageDraw.Draw(img)

    r, g, b = 35, 35, 35

    for x in xrange(300):
        draw.line((x, 0, x, 300), fill = (r, g, b))

    font = ImageFont.truetype(settings.STATIC_FONT, fontSize)
    parm = draw.textsize(imgtext, font)
    draw.text(((width - parm[0]) * 0.5, (height - parm[1]) * 0.5), imgtext, font = font, fill = (255, 255, 255))

    captchaFile = cStringIO.StringIO()
    img.save(captchaFile, "PNG")
    captchaFile.seek(0)
    data = {"imghash" : imghash, "picture" : captchaFile.read()}

    return data