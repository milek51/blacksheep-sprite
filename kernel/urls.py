# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns("kernel.views",
    url(r"^$", "home"),
    url(r"^generate/$", "upload"),
    url(r"^generate/(\w{32})/$", "download"),
    url(r"^(?P<subpage>[\w\-_]+)/$", "subpage", name = "subpage"),
    url(r"^captcha.png/666/$", "captcha", name = "captcha")
)
