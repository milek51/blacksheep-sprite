# -*- coding: utf-8 -*-

from django.db import models

from kernel.tools import _slugify

from django.conf import settings

import os
import shutil

class Sprite(models.Model):
    zipFile = models.CharField("Archive", max_length = 200)
    hashName = models.CharField("Hash", max_length = 40, editable = False)
    confirmation = models.BooleanField("Download mode", default = False)
    code = models.CharField("Code", max_length = 40, editable = False, unique = True)

    def zipFileFullPath(self):
        return u"<a href='/%s' title='%s'>%s</a>" % (self.zipFile, os.path.basename(self.zipFile), os.path.basename(self.zipFile))

    zipFileFullPath.allow_tags = True
    zipFileFullPath.short_description = "Archive"

    class Meta:
        verbose_name = "sprite"
        verbose_name_plural = "sprites"

    def save(self, *args, **kwargs):
        super(Sprite, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        zipFile = self.zipFile

        super(Sprite, self).delete(*args, **kwargs)

        try:
            os.remove("%s%s" % (settings.ROOT_DIR, zipFile))
            shutil.rmtree(os.path.dirname("%s%s" % (settings.ROOT_DIR, zipFile)))
        except OSError:
            pass

    def __str__(self):
        return str(self.pk)

    def __unicode__(self):
        return unicode(self.pk)

class Subpage(models.Model):
    title = models.CharField("Title", max_length = 150, unique = True)
    url = models.SlugField("URL", max_length = 150, editable = False)
    text = models.TextField("Text", blank = True, null = True)
    date = models.DateTimeField("Date", auto_now = True)

    class Meta:
        verbose_name = "subpage"
        verbose_name_plural = "subpages"

    def save(self, *args, **kwargs):
        self.url = _slugify(self.title)

        super(Subpage, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return (u"subpage", (), {"subpage": self.url})

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.title)