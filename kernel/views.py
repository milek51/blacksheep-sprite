# -*- coding: utf-8 -*-

from django.template import RequestContext

from django.db import IntegrityError

from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404

from django.http import HttpResponse
from django.http import Http404

from django.conf import settings

from django.views.decorators.cache import never_cache

from kernel.forms import AddZip
from kernel.forms import handle_uploaded_and_clear_file

from kernel.models import Sprite
from kernel.models import Subpage

from kernel.tools import captchaImage

import subprocess
import os
import shutil
import zipfile

from hashlib import sha1

def home(request):
    addZip = AddZip()

    return render_to_response("base.html", {"addZip" : addZip}, context_instance = RequestContext(request))

def upload(request):
    addZip = AddZip()

    zipf = None
    token_error = None

    if request.POST:
        addZip = AddZip(request.POST, request.FILES)

        if addZip.is_valid():
            try:
                token = request.session.values()
            except:
                zipf = "No session created. Turn off private browsing mode."

                data = request.POST.copy()
                data["id_zipf"] = u""
                data["captcha"] = u""
                setattr(addZip, "data", data)

                return render_to_response("base.html", {"addZip" : addZip, "zipf" : zipf}, context_instance = RequestContext(request))

            code = sha1(addZip.cleaned_data["captcha"]).hexdigest()

            if code in token:
                zipf = handle_uploaded_and_clear_file(request.FILES["zipf"])
                
                if zipf[0]:
                    zipf = zipf[1]
    
                    hashName = os.urandom(16).encode("hex")
    
                    hashPath = "%s/sprites/%s/" % (settings.MEDIA_ROOT, hashName)
                    os.mkdir(hashPath)
    
                    f = zipfile.ZipFile("%s%s" % (settings.ROOT_DIR, zipf), "r")
    
                    filelist = f.namelist()
    
                    try:
                        for name in filelist:
                            bsource = f.read(name)
        
                            nf = open("%s%s" % (hashPath, name), "w")
                            nf.write(bsource)
                            nf.close()
                    except IOError:
                        shutil.rmtree(hashPath)
                        shutil.rmtree(os.path.dirname("%s%s" % (settings.ROOT_DIR, zipf)))

                        data = request.POST.copy()
                        data["id_zipf"] = u""
                        data["captcha"] = u""
                        setattr(addZip, "data", data)
    
                        return render_to_response("base.html", {"addZip" : addZip, "zipf" : "Bad zip structure. Files only without directories."}, context_instance = RequestContext(request))
    
                    subprocess.call(["python", "%s/kernel/sprite" % settings.ROOT_DIR, hashPath, hashPath, "--crop", "--quiet"])
    
                    for name in filelist:
                        os.remove("%s%s" % (hashPath, name))
    
                    f.close()
    
                    css_pre = "%s%s.css" % (hashPath, hashName)
                    png_pre = "%s%s.png" % (hashPath, hashName)
    
                    css_post = css_pre.replace(os.path.basename(css_pre), "blacksheep-sprite.css")
                    png_post = png_pre.replace(os.path.basename(png_pre), "blacksheep-sprite.png")
    
                    os.rename(css_pre, css_post)
                    os.rename(png_pre, png_post)
    
                    nf = zipfile.ZipFile("%s%s" % (settings.ROOT_DIR, zipf.replace(".zip", "-blacksheep-sprite.zip")), "w")
    
                    os.chdir(os.path.dirname(css_post))
    
                    nf.write(os.path.basename(css_post))
                    nf.write(os.path.basename(png_post))
    
                    nf.close()
    
                    os.chdir(settings.ROOT_DIR)
    
                    nzip = zipf.replace(".zip", "-blacksheep-sprite.zip")
    
                    addZip = AddZip()
    
                    shutil.rmtree(hashPath)
                    os.remove(zipf)

                    data = request.POST.copy()
                    data["id_zipf"] = u""
                    data["captcha"] = u""
                    setattr(addZip, "data", data)

                    try:
                        sprite = Sprite(zipFile = nzip, hashName = hashName, code = code)
                        sprite.save()
                    except IntegrityError:
                        raise Http404

                    return render_to_response("base.html", {"addZip" : addZip, "sprite" : sprite, "subpage" : True}, context_instance = RequestContext(request))
                else:
                    zipf = zipf[1]
                    token_error = None
            else:
                token_error = u"Invalid token."

        data = request.POST.copy()
        data["id_zipf"] = u""
        data["captcha"] = u""
        setattr(addZip, "data", data)

        return render_to_response("base.html", {"addZip" : addZip, "zipf" : zipf, "token_error" : token_error}, context_instance = RequestContext(request))
    else:
        raise Http404

def download(request, hashName):
    sprite = Sprite.objects.get(hashName = hashName)

    if not sprite.confirmation:
        sprite.confirmation = True
        sprite.save()
        
        response = HttpResponse(open("%s%s" % (settings.ROOT_DIR, sprite.zipFile)).read(), mimetype = "application/force-download")
        response["Content-Disposition"] = "attachment; filename=%s" % os.path.basename(sprite.zipFile)
        response["X-Sendfile"] = "Blacksheep Sprite v1.0"
        
        return response
    else:
        raise Http404

def subpage(request, subpage):
    subpage = get_object_or_404(Subpage, url = subpage)

    return render_to_response("subpage.html", {"subpage" : subpage}, context_instance = RequestContext(request))

@never_cache
def captcha(request):
    captcha = captchaImage(150, 30, 10, 18)

    response = HttpResponse()
    response["Content-Type"] = "image/png"
    response.write(captcha["picture"])

    request.session["token_%s" % 666] = captcha["imghash"]

    return response

def error500(request):
    return render_to_response("base.html", {"error" : "500"}, context_instance = RequestContext(request))

def error404(request):
    return render_to_response("base.html", {"error" : "404"}, context_instance = RequestContext(request))
