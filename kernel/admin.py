# -*- coding: utf-8 -*-

from django.contrib import admin

from kernel.models import Sprite
from kernel.models import Subpage

class Sprite_Admin(admin.ModelAdmin):
    list_display = ("pk", "zipFileFullPath", "hashName", "code", "confirmation")

class Subpage_Admin(admin.ModelAdmin):
    list_display = ("title",)

    search_fields = ("title",)

admin.site.register(Sprite, Sprite_Admin)
admin.site.register(Subpage, Subpage_Admin)