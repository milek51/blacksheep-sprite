# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from django.conf.urls import url

from django.contrib.sitemaps import GenericSitemap

from kernel.models import Subpage

info_dict_subpage = {"queryset" : Subpage.objects.all(), "date_field" : "date"}

sitemaps_subpage = {"subpage": GenericSitemap(info_dict_subpage, priority = 0.8, changefreq = "daily")}

urlpatterns = patterns("", url(r"^$", "django.contrib.sitemaps.views.sitemap", {"sitemaps": sitemaps_subpage}))