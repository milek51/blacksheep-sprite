# -*- coding: utf-8 -*-

from kernel.models import Sprite

def howMuchSprites(request):
    return {"howMuchSprites" : Sprite.objects.count()}