import os
import site

site.addsitedir("") # full path to your site-packages directory (virtualenv recomended)

import sys
sys.path.append("") # full path to your app directory

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blacksheepsprite.settings")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
