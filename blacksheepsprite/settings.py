# -*- coding: utf-8 -*-

#DEBUG = True
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = ((u"", ""),)

EMAIL_USE_TLS = True
EMAIL_HOST = ""
EMAIL_PORT = 0
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""

DOMAIN_NAME = "http://blacksheepsprite.com/" 

MANAGERS = ADMINS

DATABASES = {
    "default": {
        "ENGINE": "",
        "NAME": "",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

TIME_ZONE = "Europe/Warsaw"

LANGUAGE_CODE = "en"

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

ROOT_DIR = ""

MEDIA_ROOT = "%smedia" % ROOT_DIR
MEDIA_URL = "/media/"

STATIC_ROOT = "%sstatics" % ROOT_DIR
STATIC_URL = "/static/"

STATIC_FONT = "%s/fonts/Istok-Web-Regular.ttf" % STATIC_ROOT

STATIC_TEMPLATES = "%sstatics/" % ROOT_DIR

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder"
)

SECRET_KEY = "ht5l!5aq=_$#_kqz*d-8qm-&amp;@r=6g2-pa7jpvu-5jcy7q3%89l"

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader"
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "blacksheepsprite.processors.howMuchSprites"
)

MIDDLEWARE_CLASSES = (
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.gzip.GZipMiddleware"
)

ROOT_URLCONF = "blacksheepsprite.urls"

WSGI_APPLICATION = "blacksheepsprite.wsgi.application"

TEMPLATE_DIRS = (
     "%skernel/templates" % ROOT_DIR,
) 

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "django.contrib.admin",
    "kernel",
)
