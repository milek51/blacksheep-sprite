# BlackSheep Sprite

BlackSheep Sprite - free CSS sprite generator with PNG compression. This is my first application based on Python and Django framework.
  
## Functionality

* accept ZIP archive with images only (without trees directory structure)
* build fast and optimal single PNG file
* generate CSS sprite
* compress PNG file
* return ZIP archive with PNG file and CSS sprite

## Requirements

* Python 2.6.3
* Django 1.4.5
* PIL 1.1.7
* MySQL-Python 1.2.4

## License

[BSD](http://en.wikipedia.org/wiki/BSD_licenses)

## WWW

[http://blacksheepsprite.com/](http://blacksheepsprite.com/)
